<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestRegUser;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Lcobucci\JWT\Parser;

class AuthController extends Controller
{
    /*
     * Submit login
     * */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'username'     => 'required|email',
                'password'  => 'required|min:8'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()],401);
        }

        $user = User::whereEmail($request->username)->first();
        if(empty($user)){
            return response()->json(['error' => 'Invalid email address entered.'],401);
        }

        if(!Hash::check($request->password,$user->password)){ // invalid credentials
            return response()->json(['error' => 'Incorrect password'],401);
        }


        if(Auth::attempt(['email' => $request->username,'password' => $request->password])){ // if valid user
            $user = User::whereEmail($request->username)->first();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->save();
            $success['access_token'] = $tokenResult->accessToken; // generate token
            return response()->json($success,200);
        }else{
            return response()->json(['error' => 'Unauthorized'],401);
        }
    }

    /*
     * register new user
     * */
    public function register(RequestRegUser $request)
    {
        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);

        return response()->json(['status' => 201]);
    }

    /*
     * logout user
     * */
    public function logout(Request $request)
    {
        $value = $request->bearerToken();
        $id = (new Parser())->parse($value)->getHeader('jti');

        $isUser = DB::table('oauth_access_tokens')
            ->where('id', $id)
            ->update([
                'revoked' => true
            ]);
        if($isUser){
            $success['message'] = "Successfully logged out.";
            $success['status'] = 200;
            return response()->json($success);
        }
        else{
            $error = "Something went wrong.";
            return response()->json($error);
        }
    }
}
