<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Film;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class FilmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $film = new Film();
        $film->name = 'Dead Still';
        $film->description = 'Agatha travels to Baghdad and finds that naive young archaeologist Max needs her help as a series of murders unfold in the faded grandeur of the villa where they are staying.';
        $film->release_date = new Carbon('2021-04-19');
        $film->rating = array_rand(range(1, 5));
        $film->ticket_price = $faker->randomDigitNotNull;
        $film->country_id = Country::whereName('USA')->first()->id;
        $film->photo = 'dead_still.jpg';
        $film->save();
        $film->genres()->attach([5,1,2]);

        $film = new Film();
        $film->name = 'Agatha and the Curse of Ishtar';
        $film->description = 'Agatha travels to Baghdad and finds that naive young archaeologist Max needs her help as a series of murders unfold in the faded grandeur of the villa where they are staying.';
        $film->release_date = new Carbon('2021-05-25');
        $film->rating = array_rand(range(1, 5));
        $film->ticket_price = $faker->randomDigitNotNull;
        $film->country_id = Country::whereName('USA')->first()->id;
        $film->photo = 'agatha.jpg';
        $film->save();
        $film->genres()->attach([1,3]);

        $film = new Film();
        $film->name = 'OPERATION BUFFALO';
        $film->description = 'Agatha travels to Baghdad and finds that naive young archaeologist Max needs her help as a series of murders unfold in the faded grandeur of the villa where they are staying.';
        $film->release_date = new Carbon('2021-06-17');
        $film->rating = array_rand(range(1, 5));
        $film->ticket_price = $faker->randomDigitNotNull;
        $film->country_id = Country::whereName('United Kingdom')->first()->id;
        $film->photo = 'operation_buffalo.jpg';
        $film->save();
        $film->genres()->attach([4,6]);
    }
}
