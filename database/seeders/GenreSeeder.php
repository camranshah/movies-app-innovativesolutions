<?php

namespace Database\Seeders;

use App\Models\Genre;
use Faker\Factory;
use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genre::insert([
            [
                'name' => 'Action',
            ],
            [
                'name' => 'Comedy',
            ],
            [
                'name' => 'Drama',
            ],
            [
                'name' => 'Fantasy',
            ],
            [
                'name' => 'Horror',
            ],
            [
                'name' => 'Mystery',
            ],
            [
                'name' => 'Romance',
            ],
            [
                'name' => 'Thriller',
            ],
        ]);
    }
}
