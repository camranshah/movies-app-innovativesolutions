<?php

namespace Database\Seeders;

use App\Models\Country;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::insert([
            [
                'name' => 'Pakistan',
            ],
            [
                'name' => 'USA',
            ],
            [
                'name' => 'United Kingdom',
            ],
            [
                'name' => 'Turkey',
            ],
        ]);
    }
}
