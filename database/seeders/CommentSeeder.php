<?php

namespace Database\Seeders;

use App\Models\Comment;
use Faker\Factory;
use Faker\Provider\Lorem;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $randomNumbers = range(1, 3);
        foreach ($randomNumbers as $index) {
            $userId = $randomNumbers[array_rand(range(1, 3))];
            Comment::insert([
                'film_id' => $index,
                'user_id' => $userId,
                'comment' => Lorem::sentence(),
            ]);
        }
    }
}
