<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\GenreController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register', [AuthController::class,'register']);
Route::post('/login', [AuthController::class,'login'])->name('login');

Route::middleware('auth:api')->group(function () {
    Route::post('/logout', [AuthController::class,'logout']);
});

Route::resource('film', FilmController::class);
Route::resource('comment', CommentController::class);
Route::get('country/list', [CountryController::class, 'list']);
Route::resource('country', CountryController::class);
Route::get('genre/list', [GenreController::class, 'list']);
Route::resource('genre', GenreController::class);


/*Route::group(['before' => 'auth'], function () {
});*/
